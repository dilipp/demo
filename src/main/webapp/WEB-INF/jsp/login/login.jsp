<div class="panel-body">
    <form action="login" method="post"  modelAttribute="loginForm">
        <fieldset>
            <legend>Please sign in</legend>
          <!--  <c:if test="${param.error ne null}">
			<div style="color: red">Invalid credentials.</div>
		</c:if> -->
		 <div class="dialog-row">
             <!--  ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} -->
               <!--  <label th:if="${param.error}" th:text="${session['SPRING_SECURITY_LAST_EXCEPTION'].message}" class="text-center redText">Mot de passe inconnu</label>    -->                
            </div>
           <c:if test="${not empty errorMessage}">
   					${errorMessage}
            <div class="form-group">
                <input class="form:input-large" placeholder="User Name"
                       name='username' type="text">
            </div>
            <div class="form-group">
                <input class=" form:input-large" placeholder="Password"
                       name='password' type="password" value="">
            </div>
            <input class="btn" type="submit"
                   value="Login">
        </fieldset>
        
        <a href="showForgotPassword">Forgot Password?</a>
        
    </form>
    
    
</div>