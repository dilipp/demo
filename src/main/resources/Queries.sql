CREATE TABLE  `user` (
  `user_id` varchar(20) NOT NULL DEFAULT '',
  `user_first_name` varchar(50) NOT NULL,
  `user_middle_name` varchar(25) DEFAULT NULL,
  `user_last_name` varchar(50) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_email_id` varchar(50) NOT NULL,
  `user_status` varchar(1) DEFAULT NULL,
  `user_address` varchar(200) DEFAULT NULL,
  `created_by` varchar(20) NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `failed_login_attempt` int(3) unsigned NOT NULL DEFAULT '0',
  `force_password` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `last_login_time` datetime DEFAULT NULL,
  `locked_date` datetime DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `deleted_time` datetime DEFAULT NULL,
  `deleted_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User Table';



#####################################


CREATE TABLE  `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  `role_desc` varchar(100) DEFAULT NULL,
  `role_precedence` int(10) unsigned DEFAULT NULL,
  `part_list_ids` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Role Table';


########################################################


CREATE TABLE  `user_role` (
  `user_role_id` int(15) NOT NULL AUTO_INCREMENT,
  `role_id` int(15) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='User_Role';


##################################################


CREATE TABLE  `resource` (
  `resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(45) NOT NULL,
  `resource_type` varchar(45) NOT NULL,
  `access_url` varchar(45) DEFAULT NULL,
  `parent_resource` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Resource';

#################################################


CREATE TABLE  `role_resource` (
  `role_resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `permissions` varchar(45) NOT NULL,
  `resource_not_accessible` text,
  PRIMARY KEY (`role_resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Role_Resource';


##############################

#####Backfilling Queries::

insert into `user`
select * from dat_db.`user`;


insert into `role`
select * from dat_db.role;


insert into `user_role`
select * from dat_db.user_role;


insert into `resource`
select * from dat_db.resource;

insert into `role_resource`
select * from dat_db.role_resource;

