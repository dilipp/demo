package com.interOps.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.interOps.auth.dto.MenuDTO;
import com.interOps.user.vo.UserInfo;

public class MenuTag extends TagSupport {

	
	public int doStartTag() throws JspException {
		
		 HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		 HttpSession session = request.getSession();
		 UserInfo userInfo = (UserInfo) session.getAttribute("USER_INFO");
		 List<MenuDTO> menuList = userInfo.getMenus();
		
		JspWriter out=pageContext.getOut();
		try {
			
			  out.print("<ul>"); for(MenuDTO menu : menuList) {
			  
			  out.print("<li><a href="+menu.getAccessURL()+"><span>"+menu.getMenuName()+
			  "</span></a></li>");
			  
			  } out.print("</ul>");
			 
			//out.print("hi wassup");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY; 
	}
	
	private String method() {
		return null;
	}
}
