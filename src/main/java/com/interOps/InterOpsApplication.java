package com.interOps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

@SpringBootApplication
public class InterOpsApplication {

	private static Logger logger = LogManager.getLogger();
	
	public static void main(String[] args) {
		
		SpringApplication.run(InterOpsApplication.class, args);
		logger.debug("In Main");
	}
	
	@Bean("messageSource")
	 public MessageSource resourceBundleMessageSource() {
	    ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource ();
	    resourceBundleMessageSource.setBasename("classpath:messages");
	    return resourceBundleMessageSource;
	}

}
