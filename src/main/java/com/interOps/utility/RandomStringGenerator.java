package com.interOps.utility;

public class RandomStringGenerator {

	public static String generateString(int size){
		
		String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz"; 

		 StringBuilder sb = new StringBuilder(size);
		
		 for (int i = 0; i < size; i++) { 
	            int index = (int)(alphaNumericString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(alphaNumericString 
	                          .charAt(index)); 
	        } 
		 
	 return sb.toString();	
	}
	
	
	
	
}
