package com.interOps.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LoggingAdvice {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(LoggingAdvice.class);

	@Pointcut("execution(* com.interOps.*.controller.*.*(..))")
	private void controllerAdvice(){
	}
	
	@Pointcut("execution(* com.interOps.*.service.*.*(..))")
	private void serviceAdvice(){
	}
	
	@Pointcut("execution(* com.interOps.*.dao.*.*(..))")
	private void daoAdvice(){
	}
	
	@Before("controllerAdvice()")
	public void beforeControllerAdvice(JoinPoint joinPoint){
		logger.debug("Entering to "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
	@After("controllerAdvice()")
	public void afterControllerAdvice(JoinPoint joinPoint){
		logger.debug("Exiting from "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
	@Before("serviceAdvice()")
	public void beforeServiceAdvice(JoinPoint joinPoint){
		logger.debug("Entering to "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
	@After("serviceAdvice()")
	public void afterServiceAdvice(JoinPoint joinPoint){
		logger.debug("Exiting from "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
	@Before("daoAdvice()")
	public void beforeDAOAdvice(JoinPoint joinPoint){
		logger.debug("Entering to "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
	@After("daoAdvice()")
	public void afterDAOAdvice(JoinPoint joinPoint){
		logger.debug("Exiting from "+ joinPoint.getSignature().getName()+" of "+joinPoint.getTarget().getClass());
	}
	
}
