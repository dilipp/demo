package com.interOps.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl implements EmailService{

	 @Autowired
	 private JavaMailSender emailSender;
	
	 @Override
	 public void sendMessage(String[] to, String subject, String body) {
		        SimpleMailMessage message = new SimpleMailMessage(); 
		        message.setFrom("soomansahu8@gmail.com");
		        message.setTo(to);
		        message.setSubject(subject); 
		        message.setText(body);
		        try {
		        	emailSender.send(message);
		        }catch(Exception e) {
		        	
		        }
		        
		    }
	 
	/*
	 * @Override public void sendMessageWithAttachment( String to, String subject,
	 * String text, String pathToAttachment) { // ...
	 * 
	 * MimeMessage message = emailSender.createMimeMessage();
	 * 
	 * MimeMessageHelper helper = new MimeMessageHelper(message, true);
	 * 
	 * helper.setTo(to); helper.setSubject(subject); helper.setText(text);
	 * 
	 * FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
	 * helper.addAttachment("Invoice", file);
	 * 
	 * emailSender.send(message); // ... }
	 */
	
}
