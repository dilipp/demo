package com.interOps.auth.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.interOps.auth.form.UserDTO;
import com.interOps.auth.service.IAuthService;
import com.interOps.email.EmailService;
import com.interOps.user.vo.User;
import com.interOps.user.vo.UserInfo;
import com.interOps.utility.RandomStringGenerator;

@Controller
public class LoginController {
	
	private static Logger logger = LogManager.getLogger(LoginController.class);

	@Autowired
	private IAuthService authService;
	
	@Autowired 
	private EmailService emailService;
	
	@GetMapping(path="/login")
	public String showLoginPage(HttpSession session) {
		
		logger.debug("In"+logger.getClass());
		session.invalidate();
		return "login/login";
	}
	
	
	@GetMapping(path="/doLogin")
	public String doLogin(@ModelAttribute("loginForm") UserDTO userForm, BindingResult bindingResult,HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		
		UserInfo user = authService.getLoginDetails((String)session.getAttribute("userName"));
		session.setAttribute("USER_INFO", user);
		return "login/Welcome";
	}
	
	@GetMapping(path="/loginFailed")
	public String loginFailed() {
		
		//model.addAttribute("errorMsg","Invalid Cred");
		return "login/login";
	}
	
	
	@GetMapping(path="/showForgotPassword")
	public void showForgotPassword() throws IOException {
	
		throw new IOException();
		//return "login/forgotPassword";
	}
	
	
	@PostMapping(path="/forgotPassword")
	public String forgotPassword(@ModelAttribute("loginForm") UserDTO userForm,Model model) {
	
		String userId = userForm.getUserName();
		
		UserDTO validUser = authService.saveUserPassword(userId);
		
		if(null!=validUser) {
			
			String[] to = new String [] {validUser.getEmailID()}; 
			String subject = "Password Reset";
			String body = "We have received a request for password change.\n\nPlease use new Password to login: " + validUser.getNewPassword() + "\n\n\n\nThanks,\nInterOps system\n\n\nThis is "
					+ "system generated email.Please do not reply to it.In case of any queries, please contact Admin. ";
			emailService.sendMessage(to, subject, body);
			
		}else {
			model.addAttribute("errorMessage", "Please provide valid UserID");
			return "login/forgotPassword";
		}
			
		
		return "login/sendPassword";
	}
	
	
	
	
	
	
	
}
