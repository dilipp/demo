package com.interOps.auth.service;

import com.interOps.auth.form.UserDTO;
import com.interOps.user.vo.UserInfo;

public interface IAuthService {

	public UserDTO saveUserPassword(String userId);
	
	public UserInfo getLoginDetails(String userId);
	
	
}
