package com.interOps.auth.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.interOps.auth.dto.MenuDTO;
import com.interOps.auth.form.UserDTO;
import com.interOps.user.repository.UserRepository;
import com.interOps.user.vo.Resource;
import com.interOps.user.vo.Role;
import com.interOps.user.vo.RoleResource;
import com.interOps.user.vo.User;
import com.interOps.user.vo.UserInfo;
import com.interOps.user.vo.UserRole;
import com.interOps.utility.RandomStringGenerator;


@Service
public class AuthService implements IAuthService{
	
	@Autowired
	UserRepository userRepo;
	
	
	@Override
	public UserDTO saveUserPassword(String userId) {
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		UserDTO userDTO = new UserDTO();
		
		Optional<User> user = userRepo.findById(userId);
		
		if(user.isPresent()) {
			User validUser = user.get();
			String newPass = RandomStringGenerator.generateString(8);
			String hashedPassword = passwordEncoder.encode(newPass);
			validUser.setPassword(hashedPassword);
			userRepo.save(validUser);
			userDTO.setUserName(validUser.getUserId());
			userDTO.setNewPassword(newPass);
			userDTO.setEmailID(validUser.getEmail());
			return userDTO;
		}
		
		return null;
	}

	
	
	@Override
	public UserInfo getLoginDetails(String userId) {
	
		UserInfo userInfo = new UserInfo();
		Set<RoleResource> roleResources = new HashSet<RoleResource>();
		List<MenuDTO> menus = new ArrayList<MenuDTO>();
		Optional<User> user = userRepo.findById(userId);
		if(user.isPresent()) {
			User loggedInUser = user.get();
			Set<UserRole> userRoles = loggedInUser.getUserRole();
			
			roleResources = getResources(userRoles);
			Iterator<RoleResource> roleResourceItr = roleResources.iterator();
			RoleResource roleResource = new RoleResource();
			Resource resources = new Resource();
			
			///
			while(roleResourceItr.hasNext()){
				roleResource = (RoleResource) roleResourceItr.next();
			    resources = roleResource.getResource();
			    String accessURL = resources.getAccessUrl();
				String resourceType= resources.getResourceType();
				//Set<String> authUrls = AccessController.getAuthorizedUrl(properties,userPermission);
				if(resourceType.equalsIgnoreCase("Menu") /*|| resourceType.equalsIgnoreCase("subMenu") */){
					MenuDTO menu = createMenu(resources);
					menus.add(menu);
				}
				
		         	}
			
			///
			
			
			//userInfo.setLastLogin(loggedInUser.get);
		
		}
		
		
	userInfo.setMenus(menus);	
	return userInfo;
	
	}
	
	
		

	private Set<RoleResource> getResources(Set<UserRole> userRoles){
		
		Iterator<UserRole> UserRoleItr = userRoles.iterator();
		Set<RoleResource> roleResources = new HashSet<RoleResource>();
		
		while (UserRoleItr.hasNext()) {

			UserRole userRole = (UserRole) UserRoleItr.next();

			Role role = userRole.getRole();
			
			Iterator<RoleResource> roleResourceItr = role.getRoleResource()
					.iterator();
			while (roleResourceItr.hasNext()) {
				RoleResource roleResource = (RoleResource) roleResourceItr.next();
				roleResources.add(roleResource);
			}

		}
		
		return roleResources;
	}
	
	
		private MenuDTO createMenu(Resource resource) {
		
			MenuDTO menu = new MenuDTO();
			menu.setMenuName(resource.getResourceName());
			menu.setAccessURL(resource.getAccessUrl());
			
		return menu;
		
		}
	
	
	
}
