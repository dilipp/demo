package com.interOps.auth.dto;

import java.util.List;

public class MenuDTO {

	private int parentResourceId;
	private int resourceId;
	private String accessURL;
	private String menuName;
	private int OrderNum;
	private List<MenuDTO> submenus;
	
	public int getParentResourceId() {
		return parentResourceId;
	}
	public void setParentResourceId(int parentResourceId) {
		this.parentResourceId = parentResourceId;
	}
	public int getResourceId() {
		return resourceId;
	}
	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}
	public String getAccessURL() {
		return accessURL;
	}
	public void setAccessURL(String accessURL) {
		this.accessURL = accessURL;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public int getOrderNum() {
		return OrderNum;
	}
	public void setOrderNum(int orderNum) {
		OrderNum = orderNum;
	}
	public List<MenuDTO> getSubmenus() {
		return submenus;
	}
	public void setSubmenus(List<MenuDTO> submenus) {
		this.submenus = submenus;
	}
	
	
	
}
