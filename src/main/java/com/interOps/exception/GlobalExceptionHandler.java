package com.interOps.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(Throwable.class)
    public ModelAndView handleMyException(Throwable throwableObject) {
 
		logger.debug("In GlobalExceptionHandler", throwableObject);
		ModelAndView model = new ModelAndView();
	     model.addObject("errMsg", "Sorry your request could not be processed.Please try again");
	     model.setViewName("error/error");
	     return model;
    }
	
	
	
}
