package com.interOps.defaultsecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.interOps.defaultsecurity.service.UserDetailsServiceImpl;


@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Bean
    public UserDetailsService UserDetailsServiceImpl() {
        return new UserDetailsServiceImpl();
    }
	
	
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider impl = new DaoAuthenticationProvider();
        impl.setUserDetailsService(UserDetailsServiceImpl());
        impl.setPasswordEncoder(new BCryptPasswordEncoder());
        impl.setHideUserNotFoundExceptions(false) ;
        return impl ;
    }
    
    
    @Autowired
    private SuccessHandler successHandler;
    
    @Autowired
    private FailureHandler failureHandler;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		/*
		 * auth.userDetailsService(userDetailsService())
		 * .passwordEncoder(passwordEncoder());
		 */
        auth.authenticationProvider(daoAuthenticationProvider());

    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("**/login/**").authenticated()
                .anyRequest().permitAll() .and()
               // .formLogin().loginPage("/login").usernameParameter("username").passwordParameter("password").permitAll()
                //.loginProcessingUrl("/doLogin")
                //.successForwardUrl("/doLogin")
                .formLogin().loginPage("/login").permitAll()
                .defaultSuccessUrl("/login",true)
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                //.failureUrl("/loginFailed")
                .permitAll();;
    }
    
	
	
	
}
