package com.interOps.defaultsecurity;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.interOps.user.vo.User;
import com.interOps.user.vo.UserDetail;


@Configuration
@PropertySource("classpath:application.properties")

@Component
public class SuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired
	private UserDetailsService userDetailService;
	
	@Autowired
    private Environment env;
	
	@Autowired
    private MessageSource messageSource;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		
		  User user = (User)authentication.getPrincipal(); 
		  UserDetail userDetail = (UserDetail) userDetailService.loadUserByUsername(user.getUserId());
		 
		  if(null!=userDetail) {
			  if(userDetail.isAccountLocked()) {
				  
				  request.setAttribute("errorMessage", "User Account is Locked");
				  response.sendRedirect(request.getContextPath() + "/loginFailed");
				  
			  }
			
			  HttpSession session = request.getSession();
			  session.setAttribute("userName", user.getUserId());
			  response.sendRedirect(request.getContextPath() + "/doLogin");
			  
		  }else {
			  response.sendRedirect(request.getContextPath() + "/login");
		  }
		
		
		
	}

	
	
}
