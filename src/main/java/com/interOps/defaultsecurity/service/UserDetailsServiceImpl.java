package com.interOps.defaultsecurity.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.interOps.user.repository.UserRepository;
import com.interOps.user.vo.User;
import com.interOps.user.vo.UserDetail;

public class UserDetailsServiceImpl implements UserDetailsService{

	
	@Autowired
    private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<User> optionalUsers = userRepository.findById(username);
		
		 optionalUsers
         .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
		 return optionalUsers
         .map(UserDetail::new).get();
		
	}

}
