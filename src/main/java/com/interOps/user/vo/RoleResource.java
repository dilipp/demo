package com.interOps.user.vo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="role_resource")
public class RoleResource {
	
	private int roleResourceId;
	private String permission;
	private String resourceUnavalable;
	
	private Role role;
	private Resource resource;
	
	@Id
    @Column(name = "role_resource_id")
	public int getRoleResourceId() {
		return roleResourceId;
	}
	
	public void setRoleResourceId(int roleResourceId) {
		this.roleResourceId = roleResourceId;
	}
	
	@Column(name = "permissions")
	public String getPermission() {
		return permission;
	}
	
	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	@Column(name = "resource_not_available")
	public String getResourceUnavalable() {
		return resourceUnavalable;
	}
	
	public void setResourceUnavalable(String resourceUnavalable) {
		this.resourceUnavalable = resourceUnavalable;
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="role_id")
	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	@ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="resource_id")
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
}
