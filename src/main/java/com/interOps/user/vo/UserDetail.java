package com.interOps.user.vo;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetail extends User implements UserDetails{

	 public UserDetail(final User user) {
	        super(user);
	    }
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for(UserRole userRole : getUserRole()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(userRole.getRole().getRoleName()));
		}
		
		return grantedAuthorities;
				
	}

	@Override
	public String getUsername() {
		return userId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	public boolean isAccountLocked() {
		return (!"L".equalsIgnoreCase(userStatus))?false:true;
	}
	

}
