package com.interOps.user.vo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="role")
public class Role implements Serializable{

		private static final long serialVersionUID = 1L;
	
		private int roleId;
	    private String roleName;
	    
	    private Set<UserRole> userRole;
	 
	    private Set<RoleResource> roleResource; 
	    
	    public Role() {
	    }

	    @Id
	    @Column(name = "role_id")
	    public int getRoleId() {
	        return roleId;
	    }

	    public void setRoleId(int roleId) {
	        this.roleId = roleId;
	    }

	    @Column(name = "role_name")
		public String getRoleName() {
			return roleName;
		}

		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}

		@OneToMany(mappedBy="role")
		public Set<UserRole> getUserRole() {
			return userRole;
		}

		public void setUserRole(Set<UserRole> userRole) {
			this.userRole = userRole;
		}

		@OneToMany(mappedBy="role", fetch=FetchType.EAGER)
		public Set<RoleResource> getRoleResource() {
			return roleResource;
		}

		public void setRoleResource(Set<RoleResource> roleResource) {
			this.roleResource = roleResource;
		}

	
}
