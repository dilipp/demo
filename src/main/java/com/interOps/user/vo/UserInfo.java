package com.interOps.user.vo;

import java.util.List;
import java.util.Set;

import com.interOps.auth.dto.MenuDTO;

public class UserInfo {

	
	private String userId;
	private String userName;
	private int forcePass;
	private int failedLoginAttempt;
	private String lastLogin;
	private Set<String> accessURL;
	private Set<String> resourcePermissions;
	private List<MenuDTO> menus;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getForcePass() {
		return forcePass;
	}
	public void setForcePass(int forcePass) {
		this.forcePass = forcePass;
	}
	public int getFailedLoginAttempt() {
		return failedLoginAttempt;
	}
	public void setFailedLoginAttempt(int failedLoginAttempt) {
		this.failedLoginAttempt = failedLoginAttempt;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Set<String> getAccessURL() {
		return accessURL;
	}
	public void setAccessURL(Set<String> accessURL) {
		this.accessURL = accessURL;
	}
	public Set<String> getResourcePermissions() {
		return resourcePermissions;
	}
	public void setResourcePermissions(Set<String> resourcePermissions) {
		this.resourcePermissions = resourcePermissions;
	}
	public List<MenuDTO> getMenus() {
		return menus;
	}
	public void setMenus(List<MenuDTO> menus) {
		this.menus = menus;
	}
	
	
}
