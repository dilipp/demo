package com.interOps.user.vo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	protected String userId;
    private String password;
    private String email;
    private String name;
    private String lastName;
    protected String userStatus;
    
    private Set<UserRole> userRole;
    
    
    public User() {
    }

    public User(User user) {
        this.userStatus = user.getUserStatus();
        this.email = user.getEmail();
        this.userRole = user.getUserRole();
        this.name = user.getName();
        this.lastName =user.getLastName();
        this.userId = user.getUserId();
        this.password = user.getPassword();
    }

    
    @Id
    @Column(name = "user_id")
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

    @Column(name = "user_password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "user_email_id")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
  
    @Column(name = "user_first_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "user_last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "user_status")
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}


}
