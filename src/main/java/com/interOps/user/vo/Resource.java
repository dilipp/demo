package com.interOps.user.vo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="resource")
public class Resource implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer resourceId;
	private String resourceName;
	private String resourceType;
	private String accessUrl;
	private Integer parentResource;
	private Integer orderNumber;
	
	private Set<RoleResource> roleResource;
	
	@Id
	@Column(name="resource_id")
	public Integer getResourceId() {
		return resourceId;
	}

	public void setResourceId(Integer resourceId) {
		this.resourceId = resourceId;
	}

	@Column(name="resource_name")
	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	@Column(name="resource_type")
	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@Column(name="access_url")
	public String getAccessUrl() {
		return accessUrl;
	}

	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	@Column(name="parent_resource")
	public Integer getParentResource() {
		return parentResource;
	}

	public void setParentResource(Integer parentResource) {
		this.parentResource = parentResource;
	}

	@Column(name="order_num")
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	@OneToMany(mappedBy="resource", fetch=FetchType.EAGER)
	public Set<RoleResource> getRoleResource() {
		return roleResource;
	}

	public void setRoleResource(Set<RoleResource> roleResource) {
		this.roleResource = roleResource;
	}

	
	
}
