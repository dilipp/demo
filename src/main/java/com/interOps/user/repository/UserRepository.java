package com.interOps.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.interOps.user.vo.User;


public interface UserRepository extends JpaRepository<User, String>{

	Optional<User> findByUserId(String username);
	
	/*
	 * @Query("SELECT con FROM Contact con  WHERE con.phoneType=(:pType) AND con.lastName= (:lName)"
	 * ) List<Contact> findByLastNameAndPhoneType(@Param("pType") PhoneType
	 * pType, @Param("lName") String lName);
	 */
	
}
